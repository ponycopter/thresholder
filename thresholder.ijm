//====== Thresholder
// ImageJ Macro and R Script to optimise 32bit threshold determination in microCT xylem vessel analysis.
// by Markus Nolf, m.nolf@westernsydney.edu.au, https://bitbucket.org/ponycopter/thresholder
// Use in combination with the accompanying R script.
// See notes and further information at the project page linked above.

file = File.openDialog("Open image file:");
open(file);
filename = getTitle();
dir = File.directory();
sample = File.nameWithoutExtension;

createdir = dir+"threshold-"+sample;
savedir = createdir;
File.makeDirectory(createdir);

run("Select None");
run("Set Measurements...", "area fit feret's limit redirect=None decimal=9");
print(" "); 
selectWindow("Log");
run("Close");

imgwindow = getTitle();
getMinAndMax(min,max);

iincrement = round((max-min)/400);

for(i=min;i<max; i=i+iincrement){
  
  selectWindow(imgwindow);
  run("Duplicate...", " ");
  run("Median...", "radius=0.5");
  
  setThreshold(min,i);
  run("Make Binary");
  run("Watershed");
  run("Analyze Particles...", "size=0.0002-0.01 circularity=0.50-1.00 show=[Overlay Masks] display clear summarize");
  run("Overlay Options...", "stroke=none width=0 fill=green apply");
  run("Labels...", "color=white font=9");
  
  selectWindow("Summary");
  lines = split(getInfo(), "\n");
  headings = lines[0];
  values = lines[1];
  
  averagearea = split(lines[1], "\t");
  d=2*(sqrt(averagearea[4]/PI));
  
  
  if(i==min) { print("Threshold; Area; MeanDiam; ",replace(headings,"\\t","; ")); }
  
  if(averagearea[4]>0) { 
    print(i+"; ",averagearea[4]+"; ",d+"; ",replace(values,"\\t","\; ")); 
    saveAs("Tiff", savedir+"\\"+i+".tif");
    selectWindow("Results");
    saveAs("Results", savedir+"\\"+i+".txt");
    run("Close"); 
  }
  
  
  selectWindow("Summary"); 
  run("Close"); 
  run("Close"); 
  selectWindow(imgwindow);
  resetThreshold();
}
print(""); 
selectWindow("Log");
run("Text...", "save="+savedir+"\\log.txt");
run("Close"); 
waitForUser("Done with "+sample+". \nOutput files are located in "+savedir+".");
run("Close");
