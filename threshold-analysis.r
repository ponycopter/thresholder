setwd("C:\\mn\\microCT")

analysethresholds <- function(sample,scan,whichdiameter="normal",finalthreshold=0,output=1) {
  
  totalaream2 <- read.table(paste(sample,scan,"-crosssectionalarea.txt",sep=""))$Area/1000000
  
  #install.packages("sm")
  library(sm)  
  
  thresholdfolder = paste("threshold-p",sample,scan,"/",sep="")
  
  summarylist <- read.table(paste(thresholdfolder,"log.txt",sep=""),header=TRUE, stringsAsFactors=FALSE, sep=";")
  head(summarylist)
  filenames <- summarylist$Threshold
  
  #totalarea <- data.frame(sample="EC10",aream2=0.00000725)
  #totalarea <- data.frame(sample="EC7b",aream2=0.00000739141)
  #for initial scan: 
  
  
  if(scan=="b") {
    SEMtotalaream2 <- read.table(paste("SEM-",sample,"-crosssectionalarea.txt",sep=""))$Area/1000000
    SEMparticles <- read.table(paste("SEM-",sample,"-vessels.txt",sep=""))
  }
  if(scan=="a") {
    SEMtotalaream2 <- read.table(paste(sample,"b","-crosssectionalarea.txt",sep=""))$Area/1000000
    SEMparticles <- read.table(paste("threshold-p",sample,"b/",finalthreshold,".txt",sep=""))
  }
  
  SEMparticles$DiameterNormal <- sqrt(SEMparticles$Area*1000000/pi)*2 #diameter in um^2
  SEMparticles$DiameterFeret <- sqrt(SEMparticles$Feret*SEMparticles$MinFeret*1000000) #diameter in um^2
  SEMparticles$DiameterEllipse <- sqrt(SEMparticles$Major*SEMparticles$Minor*1000000) #diameter in um^2  
  if(whichdiameter=="normal") { SEMparticles$Diameter <- SEMparticles$DiameterNormal }
  if(whichdiameter=="feret") { SEMparticles$Diameter <- SEMparticles$DiameterFeret } 
  if(whichdiameter=="ellipse") { SEMparticles$Diameter <- SEMparticles$DiameterEllipse } #diameter in um^2  
  SEMparticles$DiameterM <- SEMparticles$Diameter/1000000 #sqrt((SEMparticles$Area*10^-6)/pi)*2 #diameter in m^2
  SEMparticles$Fourthpower <- (SEMparticles$Diameter)^4
  SEMmeandh <- (sum(SEMparticles$Fourthpower)/nrow(SEMparticles))^0.25
  SEMmeandh
  SEMparticles$Ktheoretical <- (pi*(SEMparticles$DiameterM)^4)/(128*1.002*10^-9)*1000 #theoretical K [m4 MPa-1 s-1]
  sum(SEMparticles$Ktheoretical)
  SEMsumKtDh <- sum(SEMparticles$Ktheoretical)/SEMtotalaream2
  
  nrow(SEMparticles)
  SEMparticles <- SEMparticles[SEMparticles$Diameter>=17.00337,] #remove vessels smaller than picked up in CT scans
  nrow(SEMparticles)
  head(SEMparticles)
  
  #calculate SEM d95
  SEMparticles <- SEMparticles[order(SEMparticles$DiameterM,decreasing=TRUE),]
  head(SEMparticles)
  
  seq1 <- seq(145,0, by=-5)
  seq2 <- seq(150,5, by=-5)
  
  SEMnewtable <- data.frame(list(From=seq1,To=seq2,n=rep(NA,length(seq1)),Sum4thPower=rep(NA,length(seq1)),PercentConductivity=rep(NA,length(seq1)),percentVessels=rep(NA,length(seq1))))
  for(m in 1:length(seq1)) { 
    tmpdata <- SEMparticles[SEMparticles$Diameter>=SEMnewtable[m,]$From & SEMparticles$Diameter<SEMnewtable[m,]$To,]
    SEMnewtable[m,]$n <- nrow(tmpdata)
    if(nrow(tmpdata)>0) { SEMnewtable[m,]$Sum4thPower <- sum(tmpdata$Fourthpower) } else { SEMnewtable[m,]$Sum4thPower = 0 }
    SEMnewtable[m,]; m = m + 1
  }
  SEMnewtable
  
  SEMnewtable2 <- SEMnewtable
  SEMnewtable2$PercentConductivity <- SEMnewtable2$Sum4thPower/sum(SEMnewtable2$Sum4thPower)*100
  SEMnewtable2$percentVessels <- SEMnewtable2$n/sum(SEMnewtable$n)*100
  SEMnewtable2
  
  SEMmeand <- mean(SEMparticles$Diameter)
  
  tmppercentage <- 0
  j <- 0
  while(tmppercentage<95) {
    j = j+1  
    tmppercentage <- sum(SEMnewtable2[SEMnewtable2$From>seq2[j],]$PercentConductivity)
  }
  tmppercentage
  seq2[j]
  
  SEMnewtable2$UsedForD95 = 0
  SEMnewtable2[SEMnewtable2$From>seq2[j],]$UsedForD95 <- 1
  
  SEMmeand95 <- mean(SEMparticles[SEMparticles$Diameter>=seq2[j],]$Diameter)
  SEMmeand95
  
  SEMmaxtendiameters <- median(SEMparticles[1:10,]$Diameter)
  SEMmaxtendiametersavg <- mean(SEMparticles[1:10,]$Diameter)
  
  resultstable <- data.frame(list(threshold=1,nEmbolVessels=1,SumEmbolArea=10, KtDh=1, Kh=1, meanD=0.001,D95=0.001,Dh=0.001, maxtenavg=1,maxten=1), stringsAsFactors=FALSE)
  
  for(i in 1:length(filenames)) {
    currentfile <- filenames[i]
    currentdata <- read.table(paste(thresholdfolder,currentfile,".txt",sep=""))
    head(currentdata)
    currentdata$DiameterNormal <- sqrt(currentdata$Area*1000000/pi)*2 #diameter in um^2
    currentdata$DiameterFeret <- sqrt(currentdata$Feret*currentdata$MinFeret*1000000) #diameter in um^2
    currentdata$DiameterEllipse <- sqrt(currentdata$Major*currentdata$Minor*1000000) #diameter in um^2 
    if(whichdiameter=="normal") { currentdata$Diameter <- currentdata$DiameterNormal } 
    if(whichdiameter=="feret") { currentdata$Diameter <- currentdata$DiameterFeret } 
    if(whichdiameter=="ellipse") { currentdata$Diameter <- currentdata$DiameterEllipse }  
    
    currentdata$DiameterM <- currentdata$Diameter/1000000 #diameter in m^2
    currentdata$Fourthpower <- (currentdata$Diameter)^4
    currentdata$Ktheoretical <- (pi*(currentdata$DiameterM)^4)/(128*1.002*10^-9)*1000 #theoretical K [m4 MPa-1 s-1]
    sum(currentdata$Ktheoretical)
    
    currentdata <- currentdata[order(currentdata$Diameter,decreasing=TRUE),]
    head(currentdata)
    
    seq1 <- seq(145,0, by=-5)
    seq2 <- seq(150,5, by=-5)
    
    newtable <- data.frame(list(From=seq1,To=seq2,n=rep(NA,length(seq1)),Sum4thPower=rep(NA,length(seq1)),PercentConductivity=rep(NA,length(seq1)),percentVessels=rep(NA,length(seq1))))
    for(m in 1:length(seq1)) { 
      tmpdata <- currentdata[currentdata$Diameter>=newtable[m,]$From & currentdata$Diameter<newtable[m,]$To,]
      newtable[m,]$n <- nrow(tmpdata)
      if(nrow(tmpdata)>0) { newtable[m,]$Sum4thPower <- sum(tmpdata$Fourthpower) } else { newtable[m,]$Sum4thPower = 0 }
      newtable[m,]; m = m + 1
    }
    newtable
    
    newtable2 <- newtable
    newtable2$PercentConductivity <- newtable2$Sum4thPower/sum(newtable2$Sum4thPower)*100
    newtable2$percentVessels <- newtable2$n/sum(newtable$n)*100
    newtable2
    
    meand <- mean(currentdata$Diameter)
    
    tmppercentage <- 0
    j <- 0
    while(tmppercentage<95) {
      j = j+1  
      tmppercentage <- sum(newtable2[newtable2$From>seq2[j],]$PercentConductivity)
    }
    tmppercentage
    seq2[j]
    
    newtable2$UsedForD95 = 0
    newtable2[newtable2$From>seq2[j],]$UsedForD95 <- 1
    
    meand95 <- mean(currentdata[currentdata$Diameter>=seq2[j],]$Diameter)
    meand95
    
    meandh <- (sum(currentdata$Fourthpower)/nrow(currentdata))^0.25
    meandh
    
    nvessels <- nrow(currentdata)
    totalArea <- sum(currentdata$Area)*1000000
    
    meanKtDh <- (meandh*0.000001)^4*pi/(128*0.000000001002)*nvessels*1000/totalaream2
    sumKtDh <- sum(currentdata$Ktheoretical)/totalaream2 #actual KtDh
    sumKh <- sum(currentdata$Ktheoretical)
    
    maxtendiameters <- median(currentdata[1:10,]$Diameter)
    maxtendiametersavg <- mean(currentdata[1:10,]$Diameter)
    
    tmptable <- data.frame(list(threshold=currentfile,nEmbolVessels=nvessels,SumEmbolArea=round(totalArea), KtDh=sumKtDh, Kh=sumKh, meanD=round(meand,3),D95=round(meand95,3),Dh=round(meandh,3), maxtenavg=maxtendiametersavg, maxtenmed=maxtendiameters), stringsAsFactors=FALSE)
    tmptable
    resultstable[i,] <- c(currentfile,nvessels,round(totalArea), sumKtDh, sumKh, round(meand,3),round(meand95,3),round(meandh,3), round(maxtendiametersavg,2), round(maxtendiameters,2))
    
    if(scan=="b") { 
      firstlabel = "SEM"
      firstcolour = rgb(1,0,0,0.5)
      firstcol2 = rgb(1,0,0,1)
      secondlabel = "microCT final"
      secondcolour = rgb(0,0,1,0.5)
      secondcol2 = rgb(0,0,1,1)
    }
    if(scan=="a") { 
      firstlabel = "microCT final"
      firstcolour = rgb(0,0,1,0.5)
      firstcol2 = rgb(0,0,1,1)
      secondlabel = "microCT initial"
      secondcolour = rgb(0,1,0,0.5)
      secondcol2 = rgb(0,1,0,1)
    }
    par(las=2)
    hist(SEMparticles$DiameterM*1000000, main="", col=firstcolour, breaks=seq(0,120,5), freq=TRUE, yaxt="n", xlab="", ylab=""); axis(2, col=firstcol2, col.ticks=firstcol2, col.axis=firstcol2)
    if(scan=="a") abline(v=SEMmaxtendiameters, col=firstcol2, lwd=2)
    if(scan=="b") abline(v=SEMmaxtendiameters, col=firstcolour, lwd=2)
    #show normal SEM diameter also
    #par(new=TRUE); hist(SEMparticles$DiameterNormal, main="", col=rgb(0,1,0,0.5), breaks=seq(0,120,5), freq=TRUE, yaxt="n", xlab=""); axis(4,col=2)
    
    par(new=TRUE) 
    plot(NA, NA, xlim=c(0,100),ylim=c(0,100),xaxt="n",xlab="",yaxt="n",ylab="")
    text(100,100,"vessel diameter distribution",col=1,adj=1)
    text(100,95,firstlabel,col=firstcolour,adj=1)
    text(100,90,secondlabel,col=secondcolour,adj=1)
    
    text(100,10,"vertical lines:
      median of 10 largest 
      vessel diameters", col="darkgrey",adj=1)
    
    par(new=TRUE)
    hist(currentdata$Diameter, main=paste("threshold",currentfile,whichdiameter), col=secondcolour, breaks=seq(0,120,5), freq=TRUE, yaxt="n", xlab="Vessel diameter (µm)", ylab="Frequency (number of vessels)"); axis(4, col=secondcol2, col.ticks=secondcol2, col.axis=secondcol2,line=0)
    if(scan=="a") abline(v=maxtendiameters, col=secondcol2, lwd=2)
    if(scan=="b") abline(v=maxtendiameters, col=secondcolour, lwd=2)
    densitydata <- c(currentdata$Diameter,SEMparticles$Diameter)
    densitygroup <- as.factor(c(rep("CT",nrow(currentdata)),rep("SEM",nrow(SEMparticles))))
    
    if(scan=="b") {
      par(new=TRUE); plot(NA,NA,xlim=c(0,120),ylim=c(0,SEMsumKtDh*2), xlab="",xaxt="n", ylab="", yaxt="n")
      points(7,SEMsumKtDh, col=firstcol2)
      points(7,sumKtDh, col=secondcol2)
      text(0,SEMsumKtDh*2,"KtDh", adj=0)
      text(c(0,0,3),(SEMsumKtDh*2)*c(0.9,0.85,0.8),round(c(SEMsumKtDh,sumKtDh,(SEMsumKtDh-sumKtDh)),3),col=c(firstcol2,secondcol2,rgb(0,0,0,0.5)),cex=c(1,1,0.7), adj=0)
      text(2,(SEMsumKtDh*2)*0.8,expression(Delta,paste(": ")),col=rgb(0,0,0,0.5), cex=0.7, adj=1)
    }
    if(scan=="a") {
      par(new=TRUE); plot(NA,NA,xlim=c(0,120),ylim=c(0,100), xlab="",xaxt="n", ylab="", yaxt="n")
      text(0,100,"max10v", adj=0)
      text(c(0,0,3),c(90,85,80),round(c(SEMmaxtendiameters,maxtendiameters,(SEMmaxtendiameters-maxtendiameters)),3),col=c(firstcol2,secondcol2,rgb(0,0,0,0.5)),cex=c(1,1,0.7), adj=0)
      text(2,80,expression(Delta,paste(": ")),col=rgb(0,0,0,0.5), cex=0.7, adj=1)
    }
    
    
    if(output==1) {
      dev.copy(jpeg,filename=paste(thresholdfolder,"hist-",whichdiameter,"-th-",currentfile,".jpg", sep=""), width=8, height=6, res=300,units="in"); dev.off ();
    }
    
  }
  
  
  SEMtable <- resultstable[1,] #copy structure
  SEMtable[1,] <- c(0,nrow(SEMparticles),round(sum(SEMparticles$Area)*1000000),(sum(SEMparticles$Ktheoretical)/SEMtotalaream2), sum(SEMparticles$Ktheoretical), round(mean(SEMparticles$Diameter),3), round(SEMmeand95,3), round(SEMmeandh,3), round(SEMmaxtendiametersavg,2), round(SEMmaxtendiameters,2))
  if(scan=="b") SEMtable[1,1] <- "SEM"
  if(scan=="a") SEMtable[1,1] <- "CT fin"
  SEMtable
  
  resultstable[1:nrow(resultstable)+1,] <- resultstable
  rownames(resultstable) <- make.names(1:nrow(resultstable))
  resultstable[1,] <- SEMtable[1,]
  resultstable
  resultstable <- resultstable[-c(9)]
  
  if(scan=="b") {
    relevanttable <- na.omit(resultstable[(resultstable$KtDh>=(as.numeric(SEMtable$KtDh[1])*0.75) & resultstable$KtDh<=(as.numeric(SEMtable$KtDh[1])*1.5)),])
    relevanttable$lookat <- "KtDh"
  }
  if(scan=="a") {
    relevanttable <- na.omit(resultstable[(resultstable$maxten>=(as.numeric(SEMtable$maxten[1])-3) & resultstable$maxten<=(as.numeric(SEMtable$maxten[1])+3)),])
    relevanttable$lookat <- "maxten"
  }
  relevanttable
  
  if(output==1) {
    write.csv(resultstable,paste(thresholdfolder,"thresholdresults-",sample,scan,"-full.csv",sep=""), row.names=FALSE)
    write.csv(relevanttable,paste(thresholdfolder,"thresholdresults-",sample,scan,"-cropped.csv",sep=""), row.names=FALSE)
  }
  
  resultslist <- list(results=resultstable, relevant=relevanttable)
  return(resultslist)
  resultslist
} # end of function


estimateplc <- function(finaltable,finalth,initialtable,initialth) {
  head(finaltable)
  finaltable <- finaltable$results
  finalrow <- finaltable[finaltable$threshold==finalth,]
  finalrow$what="fin"
  initialtable <- initialtable$results
  initialrow <- initialtable[initialtable$threshold==initialth,]
  initialrow$what="ini"
  
  plctable <- rbind(finalrow,initialrow)
  plctable[3,c(1,10)] <- "PLC"
  plctable[3,2:5] <- plctable[2,2:5]/plctable[1,2:5]*100
  plctable
}

par(mfrow=c(1,1))

output = 1
(ec09b <- analysethresholds(sample="EC09",scan="b","normal",finalthreshold=0,output=output)) #note down best "final scan" threshold, use in next line
(ec09a <- analysethresholds(sample="EC09",scan="a","normal",finalthreshold=13049,output=output)) #note down best "initial scan" threshold

ec09PLC <- estimateplc(finaltable=ec09b,finalth=13049,initialtable=ec09a,initialth=12485)
ec09PLC #includes more parameters
ec09PLC[c(1,4)] #threshold and PLC only


