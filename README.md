# README #
 
## Introduction ##
Thresholder optimises the 32bit grey level threshold in microCT scans based on high-resolution reference SEM scans obtained from the same samples. 


###ImageJ macro: ###
Input: Cropped, cleaned microCT scan (32bit .tif file) of a xylem cross-section (remove parts that should be ignored, e.g. pith and bark).
Output: In a subfolder, thresholded image at each step + accompanying measurement table (text file with area, ellipse fit and Feret's diameter) (filename = 32bit threshold value). Output images include a green overlay (visible in ImageJ) identifying recognised particles at each threshold step.

Note: The ImageJ macro uses a 0.5 px Median filter to reduce image noise and Watershed filter to aid vessel separation, as outlined in Nolf et al. (submitted).

###R script: ###
Input: Two microCT output folders (initial and final scan) from the ImageJ macro, two text-files with cross-sectional area for microCT samples, SEM cross-sectional area + SEM vessel areas (via "Analyze Particles"); see "How to use" for details.
Output: Histogram graphs comparing final microCT scan thresholds to SEM scans, and initial microCT scan thresholds to optimised, final microCT scan threshold. Result tables in csv format (All in respective ImageJ output folders). Finally, calculated PLC for threshold-optimised initial and final microCT scans (in R).  


## How to use: ##
Example sample: "EC09" -- replace with your own sample name in the following procedure.
1. Preparation:
	* In ImageJ, make sure "Area" and "Limit to threshold" is ticked in "Analyze/Set Measurements". Optionally, also tick "Fit ellipse" and "Feret's diameter". Set decimal places to 9.
	* Double-check that the correct scale (use mm!) is embedded in the image in "Analyze/Set Scale".
	* In R, install package "sm" (for summary information)

2. SEM reference (ImageJ)
	* Measure total cross-sectional area of your SEM sample. Save the result as "SEM-EC09-crosssectionalarea.txt".
	* Measure vessel areas from your high-resolution SEM reference using "Analyze Particles". Manual thresholding is sufficient at high resolution (in this example, 1.5µm/px). Save results as "SEM-EC09-vessels.txt".

3. MicroCT scans (ImageJ)
	* Crop the "initial scan" image to the xylem cross section, black out (or white out) parts that should be ignored (e.g. pith and bark).
	* Save file as "pEC09a.tif" (p for "prepared", a for "initial scan").
	* Measure total cross-sectional area of your initial microCT scan. Save the result as "EC09a-crosssectionalarea.txt".
	* Run the ImageJ Macro "thresholder.ijm" on "pEC09a.tif". It will create a sub-folder named "threshold-pEC09a" which contains the thresholded output (image + text file).

	* Repeat the above steps for the "final scan" image, but change the file name to pEC09b.tif (b for "final scan").

4. Analysis in R
	* Start R, open threshold-analysis.r and adjust the working directory ("setwd(...)")
	* Run first analysethresholds() command (line 276) to compare SEM and microCT final scan. In the resulting table, find the KtDh that best matches the KtDh of the SEM reference (first line of the table). Alternatively, find the closest matching KtDh in the folder threshold-pEC09b by going through the "hist" graphs (top left values, circles on the left). Note down the corresponding threshold (column "threshold"). 
	* Enter this "final scan" threshold in the next call of analysethresholds() line 277 ("finalthreshold").
	* Run second analysethresholds() command (line 277) to compare microCT final and initial scans. In the resulting table, find the "maxten" value (median of largest 10 vessel diameters) that best matches the maxten value of the final scan (first line of the table). Alternatively, find the closest matching maxten in the folder threshold-pEC09a by going through the "hist" graphs (top left values, vertical lines). 
	* Enter the final and initial objects (e.g. EC09b, EC09a) and threshold values into the call of function estimateplc() (lines 279) to estimate PLC (column KtDh in resulting table; lines 280-281).
